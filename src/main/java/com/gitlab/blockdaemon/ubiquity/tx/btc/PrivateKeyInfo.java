package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.gitlab.blockdaemon.ubiquity.tx.btc.util.Base58;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class PrivateKeyInfo {
	private final boolean testNet;
	private final Type type;
	private final  String privateKeyEncoded;
	private final  BigInteger privateKeyDecoded;
	private final  boolean isPublicKeyCompressed;
	public static final BigInteger LARGEST_PRIVATE_KEY = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", 16);

	public enum Type{
		TYPE_WIF, TYPE_MINI, TYPE_BRAIN_WALLET, TYPE_BIP38;
	}

	public static PrivateKeyInfo decode(String encodedPrivateKey) {
		return decode(encodedPrivateKey, false);
	}

	/**
	 * Decodes given string as private key
	 *
	 * @param encodedPrivateKey a text what is likely a private key
	 * @return decoded private key and its information
	 */
	public static PrivateKeyInfo decode(String encodedPrivateKey, boolean preferCompressedPublicKeyForPaperWallets) {
		if (encodedPrivateKey.length() > 0) {
			try {
				final byte[] decoded = Base58.decode(encodedPrivateKey);
				if (decoded != null && (decoded.length == 37 || decoded.length == 38) && ((decoded[0] & 0xff) == 0x80 || (decoded[0] & 0xff) == 0xef)) {
					if (verifyDoubleSha256Checksum(decoded)) {
						final boolean testNet = (decoded[0] & 0xff) == 0xef;
						final byte[] secret = new byte[32];
						System.arraycopy(decoded, 1, secret, 0, secret.length);
						boolean isPublicKeyCompressed;
						if (decoded.length == 38) {
							if (decoded[decoded.length - 5] == 1) {
								isPublicKeyCompressed = true;
							} else {
								return null;
							}
						} else {
							isPublicKeyCompressed = false;
						}
						final BigInteger privateKeyBigInteger = new BigInteger(1, secret);
						if (privateKeyBigInteger.compareTo(BigInteger.ONE) > 0 && privateKeyBigInteger.compareTo(LARGEST_PRIVATE_KEY) < 0) {
							return new PrivateKeyInfo(testNet, PrivateKeyInfo.Type.TYPE_WIF, encodedPrivateKey, privateKeyBigInteger, isPublicKeyCompressed);
						}
					}
				} else if (decoded != null && decoded.length == 43 && (decoded[0] & 0xff) == 0x01 && ((decoded[1] & 0xff) == 0x43 || (decoded[1] & 0xff) == 0x42)) {
					if (verifyDoubleSha256Checksum(decoded)) {
						return new PrivateKeyInfo(false, PrivateKeyInfo.Type.TYPE_BIP38, encodedPrivateKey, null, false);
					}
				}
			} catch (final Exception ignored) {
			}
		}
		return decodePrivateKeyAsSha256(encodedPrivateKey, false, preferCompressedPublicKeyForPaperWallets);
	}

	public static PrivateKeyInfo decodeAsSha256(String encodedPrivateKey, boolean testNet) {
		return decodePrivateKeyAsSha256(encodedPrivateKey, testNet, false);
	}

	/**
	 * Decodes brainwallet and mini keys. Both are SHA256(input), but mini keys have basic checksum verification.
	 *
	 * @param encodedPrivateKey input
	 * @return private key what is SHA256 of the input string
	 */
	public static PrivateKeyInfo decodePrivateKeyAsSha256(String encodedPrivateKey, boolean testNet, boolean isPublicKeyCompressed) {
		if (encodedPrivateKey.length() > 0) {
			try {
				final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
				final BigInteger privateKeyBigInteger = new BigInteger(1, sha256.digest(encodedPrivateKey.getBytes()));
				if (privateKeyBigInteger.compareTo(BigInteger.ONE) > 0 && privateKeyBigInteger.compareTo(LARGEST_PRIVATE_KEY) < 0) {
					Type type;

					if (sha256.digest((encodedPrivateKey + '?').getBytes("UTF-8"))[0] == 0) {
						type = PrivateKeyInfo.Type.TYPE_MINI;
					} else {
						type = PrivateKeyInfo.Type.TYPE_BRAIN_WALLET;
					}
					return new PrivateKeyInfo(testNet, type, encodedPrivateKey, privateKeyBigInteger, isPublicKeyCompressed);
				}
			} catch (final Exception ignored) {
			}
		}
		return null;
	}


	public static boolean verifyDoubleSha256Checksum(byte[] bytesWithChecksumm) {
		try {
			if (bytesWithChecksumm == null || bytesWithChecksumm.length < 5) {
				return false;
			}
			final MessageDigest digestSha = MessageDigest.getInstance("SHA-256");
			digestSha.update(bytesWithChecksumm, 0, bytesWithChecksumm.length - 4);
			final byte[] first = digestSha.digest();
			final byte[] calculatedDigest = digestSha.digest(first);
			boolean checksumValid = true;
			for (int i = 0; i < 4; i++) {
				if (calculatedDigest[i] != bytesWithChecksumm[bytesWithChecksumm.length - 4 + i]) {
					checksumValid = false;
					break;
				}
			}
			return checksumValid;
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
}
