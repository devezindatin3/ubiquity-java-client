package com.gitlab.blockdaemon.ubiquity.tx.btc;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERSequenceGenerator;
import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.math.ec.ECPoint;

import com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions.BitcoinException;
import com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions.NotImplementedException;
import com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions.ScriptInvalidException;
import com.gitlab.blockdaemon.ubiquity.tx.btc.util.Base58;


public class BtcService {

	private static final ECDomainParameters EC_PARAMS;
	private static final char[] BASE58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz".toCharArray();
	public static final TrulySecureRandom SECURE_RANDOM = new TrulySecureRandom();
	static final BigInteger LARGEST_PRIVATE_KEY = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", 16);//SECP256K1_N
	public static final long MAX_ALLOWED_FEE = BtcService.parseValue("0.1");
	public static final float EXPECTED_BLOCKS_PER_DAY = 144.0f;
	private static final int MAX_SCRIPT_ELEMENT_SIZE = 520;

	public static final int TRANSACTION_TYPE_LEGACY = 0;
	public static final int TRANSACTION_TYPE_BITCOIN_CASH = 1;
	public static final int TRANSACTION_TYPE_SEGWIT = 2;

	static {
		final X9ECParameters params = SECNamedCurves.getByName("secp256k1");
		EC_PARAMS = new ECDomainParameters(params.getCurve(), params.getG(), params.getN(), params.getH());
	}


	public static long parseValue(String valueStr) throws NumberFormatException {
		return new BigDecimal(valueStr).multiply(BigDecimal.valueOf(1_0000_0000)).setScale(0, RoundingMode.HALF_DOWN).longValueExact();
	}

	public static long calcMinimumFee(int txSizeVBytes, float satoshisPerVirtualByte) {
		return (long) (txSizeVBytes * satoshisPerVirtualByte);
	}

	public static int getMaximumTxSize(Collection<UnspentOutputInfo> unspentOutputInfos, int outputsCount, boolean compressedPublicKey) throws BitcoinException {
		if (unspentOutputInfos == null || unspentOutputInfos.isEmpty()) {
			throw new BitcoinException(BitcoinException.ERR_NO_INPUT, "No information about tx inputs provided");
		}
		final int maxInputScriptLen = 73 + (compressedPublicKey ? 33 : 65);
		return 9 + unspentOutputInfos.size() * (41 + maxInputScriptLen) + outputsCount * 33;
	}


	public static boolean verifyDoubleSha256Checksum(byte[] bytesWithChecksumm) {
		try {
			if (bytesWithChecksumm == null || bytesWithChecksumm.length < 5) {
				return false;
			}
			final MessageDigest digestSha = MessageDigest.getInstance("SHA-256");
			digestSha.update(bytesWithChecksumm, 0, bytesWithChecksumm.length - 4);
			final byte[] first = digestSha.digest();
			final byte[] calculatedDigest = digestSha.digest(first);
			boolean checksumValid = true;
			for (int i = 0; i < 4; i++) {
				if (calculatedDigest[i] != bytesWithChecksumm[bytesWithChecksumm.length - 4 + i]) {
					checksumValid = false;
					break;
				}
			}
			return checksumValid;
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] sha256ripemd160(byte[] publicKey) {
		try {
			final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			//https://en.bitcoin.it/wiki/Technical_background_of_Bitcoin_addresses
			final byte[] sha256hash = sha256.digest(publicKey);
			final RIPEMD160Digest ripemd160Digest = new RIPEMD160Digest();
			ripemd160Digest.update(sha256hash, 0, sha256hash.length);
			final byte[] hashedPublicKey = new byte[20];
			ripemd160Digest.doFinal(hashedPublicKey, 0);
			return hashedPublicKey;
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}


	public static KeyPair generateMiniKey(Address.Type publicKeyRepresentation) {
		KeyPair key = null;
		try {
			final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			final StringBuilder sb = new StringBuilder(31);
			SECURE_RANDOM.addSeedMaterial(System.currentTimeMillis() );
			while (true) {
				sb.append('S');
				for (int i = 0; i < 29; i++) {
					sb.append(BASE58[1 + SECURE_RANDOM.nextInt(BASE58.length - 1)]);
				}
				if (sha256.digest((sb.toString() + '?').getBytes("UTF-8"))[0] == 0) {
					final boolean compressedPublicKey = publicKeyRepresentation != Address.Type.PUBLIC_KEY_TO_ADDRESS_LEGACY;
					final PrivateKeyInfo pk = PrivateKeyInfo.decodePrivateKeyAsSha256(sb.toString(), false, compressedPublicKey);
					if (pk != null) {
						key = new KeyPair(pk, publicKeyRepresentation);
					}
					break;
				}
				sb.setLength(0);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return key;
	}

	public static KeyPair generateWifKey(boolean testNet, Address.Type publicKeyRepresentation) {
		SECURE_RANDOM.addSeedMaterial(System.currentTimeMillis() );
		try {
			final MessageDigest digestSha = MessageDigest.getInstance("SHA-256");
			final byte[] rawPrivateKey = new byte[38];
			rawPrivateKey[0] = (byte) (testNet ? 0xef : 0x80);
			rawPrivateKey[rawPrivateKey.length - 5] = 1;
			byte[] secret;
			BigInteger privateKeyBigInteger;
			do {
				secret = new byte[32];
				SECURE_RANDOM.nextBytes(secret);
				privateKeyBigInteger = new BigInteger(1, secret);
				System.arraycopy(secret, 0, rawPrivateKey, 1, secret.length);
				digestSha.update(rawPrivateKey, 0, rawPrivateKey.length - 4);
				final byte[] check = digestSha.digest(digestSha.digest());
				System.arraycopy(check, 0, rawPrivateKey, rawPrivateKey.length - 4, 4);
			}
			while (privateKeyBigInteger.compareTo(BigInteger.ONE) < 0 || privateKeyBigInteger.compareTo(LARGEST_PRIVATE_KEY) > 0 || !verifyDoubleSha256Checksum(rawPrivateKey));
			final PrivateKeyInfo privateKeyInfo = new PrivateKeyInfo(testNet, PrivateKeyInfo.Type.TYPE_WIF, Base58.encode(rawPrivateKey),
					privateKeyBigInteger, true);
			return new KeyPair(privateKeyInfo, publicKeyRepresentation);
		} catch (final NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static String encodeWifKey(boolean isPublicKeyCompressed, byte[] secret, boolean testNet) {
		try {
			final MessageDigest digestSha = MessageDigest.getInstance("SHA-256");
			final byte[] rawPrivateKey = new byte[isPublicKeyCompressed ? 38 : 37];
			rawPrivateKey[0] = (byte) (testNet ? 0xef : 0x80);
			if (isPublicKeyCompressed) {
				rawPrivateKey[rawPrivateKey.length - 5] = 1;
			}
			System.arraycopy(secret, 0, rawPrivateKey, 1, secret.length);
			digestSha.update(rawPrivateKey, 0, rawPrivateKey.length - 4);
			final byte[] check = digestSha.digest(digestSha.digest());
			System.arraycopy(check, 0, rawPrivateKey, rawPrivateKey.length - 4, 4);
			return Base58.encode(rawPrivateKey);
		} catch (final NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static String toHex(byte[] bytes) {
		if (bytes == null) {
			return "";
		}
		final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
		final char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}


	public static byte[] fromHex(String s) {
		if (s != null) {
			try {
				final StringBuilder sb = new StringBuilder(s.length());
				for (int i = 0; i < s.length(); i++) {
					final char ch = s.charAt(i);
					if (!Character.isWhitespace(ch)) {
						sb.append(ch);
					}
				}
				s = sb.toString();
				final int len = s.length();
				final byte[] data = new byte[len / 2];
				for (int i = 0; i < len; i += 2) {
					final int hi = (Character.digit(s.charAt(i), 16) << 4);
					final int low = Character.digit(s.charAt(i + 1), 16);
					if (hi >= 256 || low < 0 || low >= 16) {
						return null;
					}
					data[i / 2] = (byte) (hi | low);
				}
				return data;
			} catch (final Exception ignored) {
			}
		}
		return null;
	}


	public static byte[] fromValidHex(String s) {
		final StringBuilder sb = new StringBuilder(s.length());
		for (int i = 0; i < s.length(); i++) {
			final char ch = s.charAt(i);
			if (!Character.isWhitespace(ch)) {
				sb.append(ch);
			}
		}
		s = sb.toString();
		final int len = s.length();
		final byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			final int hi = (Character.digit(s.charAt(i), 16) << 4);
			final int low = Character.digit(s.charAt(i + 1), 16);
			if (hi >= 256 || low < 0 || low >= 16) {
				throw new RuntimeException("invalid hex input");
			}
			data[i / 2] = (byte) (hi | low);
		}
		return data;
	}

	public static byte[] sign(BigInteger privateKey, byte[] input) {
		synchronized (EC_PARAMS) {
			final ECDSASigner signer = new ECDSASigner();
			final ECPrivateKeyParameters privateKeyParam = new ECPrivateKeyParameters(privateKey, EC_PARAMS);
			signer.init(true, new ParametersWithRandom(privateKeyParam, SECURE_RANDOM));
			final BigInteger[] sign = signer.generateSignature(input);
			final BigInteger r = sign[0];
			BigInteger s = sign[1];
			final BigInteger largestAllowedS = new BigInteger("7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D576E7357A4501DDFE92F46681B20A0", 16);
			if (s.compareTo(largestAllowedS) > 0) {
				//https://github.com/bitcoin/bips/blob/master/bip-0062.mediawiki#low-s-values-in-signatures
				s = LARGEST_PRIVATE_KEY.subtract(s);
			}
			try {
				final ByteArrayOutputStream baos = new ByteArrayOutputStream(72);
				final DERSequenceGenerator derGen = new DERSequenceGenerator(baos);
				derGen.addObject(new ASN1Integer(r));
				derGen.addObject(new ASN1Integer(s));
				derGen.close();
				return baos.toByteArray();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static boolean verify(byte[] publicKey, byte[] signature, byte[] msg) {
		synchronized (EC_PARAMS) {
			boolean valid;
			final ECDSASigner signerVer = new ECDSASigner();
			if (publicKey.length == 0) {
				return false;
			}
			if (signature.length == 0) {
				return true;
			}
			final ECPublicKeyParameters pubKey = new ECPublicKeyParameters(EC_PARAMS.getCurve().decodePoint(publicKey), EC_PARAMS);
			signerVer.init(false, pubKey);
			BigInteger r, s;
			try {
				final ASN1InputStream derSigStream = new ASN1InputStream(signature);
				final DLSequence seq = (DLSequence) derSigStream.readObject();
				r = ((ASN1Integer) seq.getObjectAt(0)).getPositiveValue();
				s = ((ASN1Integer) seq.getObjectAt(1)).getPositiveValue();
				derSigStream.close();
			} catch (final Exception e) {
				try {
					int i = 0;
					if (signature[i++] != 0x30) {
						throw new RuntimeException("No ASN1 sequence in signature");
					}
					int len = signature[i] & 0xff;
					i++;
					if (i + len != signature.length) {
						throw new RuntimeException("Invalid signature ASN1 length");
					}
					byte type = signature[i];
					i++;
					if (type != 2) {
						throw new RuntimeException("R value has invalid type in signature: " + type);
					}
					len = signature[i++] & 0xff;
					final byte[] rBytes = new byte[len];
					System.arraycopy(signature, i, rBytes, 0, len);
					r = new BigInteger(1, rBytes);
					i += len;

					type = signature[i++];
					if (type != 2) {
						throw new RuntimeException("S value has invalid type in signature: " + type);
					}
					len = signature[i++] & 0xff;

					System.arraycopy(signature, i, rBytes, 0, len);
					s = new BigInteger(1, rBytes);
				} catch (final Exception err2) {
					throw new RuntimeException("Invalid ASN/DER encoding of signature", err2);
				}
			}
			valid = signerVer.verifySignature(msg, r, s);
			return valid;
		}
	}

	public static byte[] reverse(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		final byte[] result = new byte[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			result[i] = bytes[bytes.length - i - 1];
		}
		return result;
	}

	public static byte[] reverseInPlace(byte[] bytes) {
		final int len = bytes.length / 2;
		for (int i = 0; i < len; i++) {
			final byte t = bytes[i];
			bytes[i] = bytes[bytes.length - i - 1];
			bytes[bytes.length - i - 1] = t;
		}
		return bytes;
	}

	public static int findSpendableOutput(BtcTransaction tx, String forAddress, long minAmount) throws BitcoinException {
		final byte[] outputScriptWeAreAbleToSpend = Script.buildOutput(forAddress).bytes;
		int indexOfOutputToSpend = -1;
		for (int indexOfOutput = 0; indexOfOutput < tx.getOutputs().length; indexOfOutput++) {
			final BtcTransaction.Output output = tx.getOutputs()[indexOfOutput];
			if (Arrays.equals(outputScriptWeAreAbleToSpend, output.scriptPubKey.bytes)) {
				indexOfOutputToSpend = indexOfOutput;
				break;//only one input is supported for now
			}
		}
		if (indexOfOutputToSpend == -1) {
			throw new BitcoinException(BitcoinException.ERR_NO_SPENDABLE_OUTPUTS_FOR_THE_ADDRESS, "No spendable standard outputs for " + forAddress + " have found", forAddress);
		}
		final long spendableOutputValue = tx.getOutputs()[indexOfOutputToSpend].value;
		if (spendableOutputValue < minAmount) {
			throw new BitcoinException(BitcoinException.ERR_INSUFFICIENT_FUNDS, "Unspent amount is too small: " + spendableOutputValue, spendableOutputValue);
		}
		return indexOfOutputToSpend;
	}

	public static void verify(Script[] scriptPubKeys, long[] amounts, BtcTransaction spendTx, boolean bitcoinCash) throws ScriptInvalidException {
		int flags = Script.SCRIPT_ALL_SUPPORTED;
		if (bitcoinCash) {
			flags |= Script.SCRIPT_ENABLE_SIGHASH_FORKID;
		}
		verify(scriptPubKeys, amounts, spendTx, flags);
	}

	public static void verify(Script[] scriptPubKeys, long[] amounts, BtcTransaction tx, int flags) throws ScriptInvalidException {
		if (tx.isCoinBase()) {
			throw new NotImplementedException("Coinbase verification");
		}
		for (int i = 0; i < tx.getOutputs().length; i++) {
			if (tx.getOutputs()[i].value < 0) {
				throw new ScriptInvalidException("Negative output");
			}
		}
		final HashSet<BtcTransaction.OutPoint> inputsPointsSet = new HashSet<>(tx.getInputs().length);
		for (int i = 0; i < tx.getInputs().length; i++) {
			if (!inputsPointsSet.add(tx.getInputs()[i].getOutPoint())) {
				throw new ScriptInvalidException("Duplicate inputs");
			}
		}
		for (int i = 0; i < scriptPubKeys.length; i++) {
			if (scriptPubKeys[i] == null || amounts[i] < 0) {
				//verify only given inputs
				continue;
			}
			final BtcTransaction.Checker checker = new BtcTransaction.Checker(i, amounts[i], tx);
			final Stack<byte[]> stack = new Stack<>();
			Stack<byte[]> stackCopy = null;
			final Script scriptSig = tx.getInputs()[i].getScriptSig();
			if ((flags & Script.SCRIPT_VERIFY_SIGPUSHONLY) != 0 && !scriptSig.isPushOnly()) {
				throw new ScriptInvalidException("SCRIPT_ERR_SIG_PUSHONLY");
			}
			if (scriptSig.isNull() && tx.getInputs().length > 1 && !tx.isCoinBase() && (flags & Script.SCRIPT_VERIFY_WITNESS) == 0) {
				throw new ScriptInvalidException("Null txin, but without being a coinbase (because there are two inputs)");
			}
			if (!scriptSig.run(checker, stack, flags, Script.SIGVERSION_BASE)) { //usually loads signature+public key
				throw new ScriptInvalidException();
			}
			if ((flags & Script.SCRIPT_VERIFY_P2SH) != 0) {
				stackCopy = new Stack<>();
				stackCopy.addAll(stack);
			}
			final Script scriptPubKey = scriptPubKeys[i];
			if (!scriptPubKey.run(checker, stack, flags, Script.SIGVERSION_BASE)) { //verify that this transaction able to spend that output
				throw new ScriptInvalidException();
			}
			if (stack.isEmpty() || !castToBool(stack.peek())) {
				throw new ScriptInvalidException();
			}
			// Bare witness programs
			boolean hadWitness = false;
			if ((flags & Script.SCRIPT_VERIFY_WITNESS) != 0) {
				final Script.WitnessProgram wp = scriptPubKey.getWitnessProgram();
				if (wp != null) {
					hadWitness = true;
					if (scriptSig.bytes.length != 0) {
						// The scriptSig must be _exactly_ CScript(), otherwise we reintroduce malleability.
						throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_MALLEATED");
					}
					final byte[][] witness = i < tx.getScriptWitnesses().length ? tx.getScriptWitnesses()[i] : new byte[0][];
					if (!verifyWitnessProgram(checker, witness, wp, flags)) {
						throw new ScriptInvalidException("Bad signature in witness");
					}
					// Bypass the cleanstack check at the end. The actual stack is _obviously_ not clean
					// for witness programs.
					stack.clear();
					stack.add(null);
				}
			}
			if ((flags & Script.SCRIPT_VERIFY_P2SH) != 0 && scriptPubKey.isPayToScriptHash()) {
				if (!scriptSig.isPushOnly()) {
					throw new ScriptInvalidException("SCRIPT_ERR_SIG_PUSHONLY");
				}
				stack.clear();
				if (stackCopy == null) {
					throw new RuntimeException("No stack copy in P2SH verification");
				}
				stack.addAll(stackCopy);
				final byte[] pubKeySerialized = stack.pop();
				Script pubKey2;
				try {
					pubKey2 = new Script(pubKeySerialized);
					if (!pubKey2.run(checker, stack, flags, Script.SIGVERSION_BASE)) {
						throw new ScriptInvalidException();
					}
					if (stack.isEmpty() || !castToBool(stack.pop())) {
						throw new ScriptInvalidException();
					}

					if ((flags & Script.SCRIPT_VERIFY_WITNESS) != 0) {
						final Script.WitnessProgram wp = pubKey2.getWitnessProgram();
						if (wp != null) {
							hadWitness = true;
							if (!Arrays.equals(scriptSig.bytes, Script.convertDataToScript(pubKey2.bytes))) {
								// The scriptSig must be _exactly_ CScript(), otherwise we reintroduce malleability.
								throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_MALLEATED");
							}
							if (!verifyWitnessProgram(checker, tx.getScriptWitnesses()[i], wp, flags)) {
								throw new ScriptInvalidException("Bad witness");
							}
							// Bypass the cleanstack check at the end. The actual stack is _obviously_ not clean
							// for witness programs.
							stack.clear();
							stack.add(null);
						}
					}
				} catch (final NotImplementedException e) {
					throw e;
				} catch (final ScriptInvalidException e) {
					throw e;
				} catch (final Exception e) {
					throw new ScriptInvalidException(e.toString());
				}
			}

			if (((flags & Script.SCRIPT_VERIFY_CLEANSTACK) != 0) && (stack.size() != 1)) {
				throw new ScriptInvalidException("SCRIPT_ERR_CLEANSTACK");
			}

			if ((flags & Script.SCRIPT_VERIFY_WITNESS) != 0) {
				if (!hadWitness && tx.getScriptWitnesses().length > 0 && tx.getScriptWitnesses()[i].length > 0) {
					throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_UNEXPECTED");
				}
			} else if (tx.getScriptWitnesses().length > 0) {
				throw new NotImplementedException("SegWit is not supported yet");
			}
		}
	}

	private static boolean verifyWitnessProgram(BtcTransaction.Checker checker, byte[][] scriptWitnesses, Script.WitnessProgram wp, int flags)
			throws ScriptInvalidException {
		final Stack<byte[]> stack = new Stack<>();
		Script scriptPubKey;
		if (wp.version == 0) {
			if (wp.isWitnessSha256Type()) {
				// Version 0 segregated witness program: SHA256(CScript) inside the program, CScript + inputs in witness
				if (scriptWitnesses.length == 0) {
					throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_PROGRAM_WITNESS_EMPTY");
				}
				scriptPubKey = new Script(scriptWitnesses[scriptWitnesses.length - 1]);
				final byte[] hashScriptPubKey = BtcService.sha256(scriptPubKey.bytes);
				if (!Arrays.equals(hashScriptPubKey, wp.program)) {
					throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_PROGRAM_MISMATCH");
				}
				for (int i = 0; i < scriptWitnesses.length - 1; i++) {
					stack.add(scriptWitnesses[i]);
				}
			} else if (wp.isWitnessKeyHashType()) {
				if (scriptWitnesses.length != 2) {
					throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_PROGRAM_MISMATCH"); // 2 items in witness
				}
				try {
					final ByteArrayOutputStream os = new ByteArrayOutputStream();
					os.write(Script.OP_DUP);
					os.write(Script.OP_HASH160);
					os.write(Script.convertDataToScript(wp.program));
					os.write(Script.OP_EQUALVERIFY);
					os.write(Script.OP_CHECKSIG);
					os.close();
					scriptPubKey = new Script(os.toByteArray());
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
				stack.addAll(Arrays.asList(scriptWitnesses));
			} else {
				throw new ScriptInvalidException("SCRIPT_ERR_WITNESS_PROGRAM_WRONG_LENGTH");
			}
		} else if ((flags & Script.SCRIPT_VERIFY_DISCOURAGE_UPGRADABLE_WITNESS_PROGRAM) != 0) {
			throw new ScriptInvalidException("SCRIPT_ERR_DISCOURAGE_UPGRADABLE_WITNESS_PROGRAM");
		} else {
			return true;
		}

		for (int i = 0; i < stack.size(); i++) {
			if (stack.get(i).length > MAX_SCRIPT_ELEMENT_SIZE) {
				throw new ScriptInvalidException("SCRIPT_ERR_PUSH_SIZE");
			}
		}

		if (!scriptPubKey.run(checker, stack, flags, Script.SIGVERSION_WITNESS_V0)) {
			return false;
		}

		// Scripts inside witness implicitly require cleanstack behaviour
		if (stack.size() != 1 || !castToBool(stack.peek())) {
			throw new ScriptInvalidException("SCRIPT_ERR_EVAL_FALSE");
		}
		return true;
	}

	public static boolean castToBool(byte[] vch) {
		for (int i = 0; i < vch.length; i++) {
			if (vch[i] != 0) {
				return ((i != vch.length - 1) || ((vch[i] & 0xFF) != 0x80));
			}
		}
		return false;
	}

	public static BtcTransaction createTransaction(BtcTransaction baseTransaction, int indexOfOutputToSpend,  String outputAddress, String changeAddress,
			long amountToSend, float satoshisPerVirtualByte, KeyPair keys,  BtcTransaction.Type transactionType) throws BitcoinException {
		final byte[] hashOfPrevTransaction = baseTransaction.hash();
		return createTransaction(hashOfPrevTransaction, baseTransaction.getOutputs()[indexOfOutputToSpend].value, baseTransaction.getOutputs()[indexOfOutputToSpend].scriptPubKey,
				indexOfOutputToSpend, outputAddress, changeAddress, amountToSend, satoshisPerVirtualByte, keys, transactionType);
	}

	public static BtcTransaction createTransaction(byte[] hashOfPrevTransaction, long valueOfUnspentOutput, Script scriptOfUnspentOutput,
			int indexOfOutputToSpend,  String outputAddress, String changeAddress, long amountToSend,
			float satoshisPerVirtualByte, KeyPair keys,  BtcTransaction.Type transactionType) throws BitcoinException {
		if (hashOfPrevTransaction == null) {
			throw new BitcoinException(BitcoinException.ERR_NO_INPUT, "hashOfPrevTransaction is null");
		}
		final ArrayList<UnspentOutputInfo> unspentOutputs = new ArrayList<>(1);
		unspentOutputs.add(new UnspentOutputInfo(keys, hashOfPrevTransaction, scriptOfUnspentOutput, valueOfUnspentOutput, indexOfOutputToSpend));
		return createTransaction(unspentOutputs, outputAddress, changeAddress, amountToSend, satoshisPerVirtualByte, transactionType);
	}

	public static BtcTransaction createTransaction(List<UnspentOutputInfo> unspentOutputs,
			String outputAddress, String changeAddress,
			final long amountToSend, final float satoshisPerVirtualByte,
			BtcTransaction.Type transactionType) throws BitcoinException {

		final boolean acceptSegWitAddresses = transactionType == BtcTransaction.Type.TRANSACTION_TYPE_SEGWIT;
		if (!Address.verify(outputAddress, acceptSegWitAddresses)) {
			throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Output address is invalid", outputAddress);
		}

		final FeeChangeAndSelectedOutputs processedTxData = calcFeeChangeAndSelectOutputsToSpend(unspentOutputs, amountToSend, satoshisPerVirtualByte);

		BtcTransaction.Output[] outputs;
		if (processedTxData.getChange() == 0) {
			outputs = new BtcTransaction.Output[]{
					new BtcTransaction.Output(processedTxData.getAmountForRecipient(), Script.buildOutput(outputAddress)),
			};
		} else {
			if (outputAddress.equals(changeAddress)) {
				throw new BitcoinException(BitcoinException.ERR_MEANINGLESS_OPERATION, "Change address equals to recipient's address, it is likely an error.");
			}
			if (!Address.verify(changeAddress, acceptSegWitAddresses)) {
				throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Change address is invalid", changeAddress);
			}
			outputs = new BtcTransaction.Output[]{
					new BtcTransaction.Output(processedTxData.getAmountForRecipient(), Script.buildOutput(outputAddress)),
					new BtcTransaction.Output(processedTxData.getChange(), Script.buildOutput(changeAddress)),
			};
		}
		final ArrayList<UnspentOutputInfo> outputsToSpend = processedTxData.getOutputsToSpend();
		final BtcTransaction.Input[] unsignedInputs = new BtcTransaction.Input[outputsToSpend.size()];
		final BtcTransaction unsignedTx = new BtcTransaction(unsignedInputs, outputs, 0);
		for (int j = 0; j < unsignedTx.getInputs().length; j++) {
			final UnspentOutputInfo outputToSpend = outputsToSpend.get(j);
			final BtcTransaction.OutPoint outPoint = new BtcTransaction.OutPoint(outputToSpend.txHash, outputToSpend.outputIndex);
			unsignedTx.getInputs()[j] = new BtcTransaction.Input(outPoint, null, 0xffffffff);
		}

		return sign(outputsToSpend, unsignedTx, transactionType);
	}


	public static BtcTransaction sign(List<UnspentOutputInfo> outputsToSpend, BtcTransaction unsignedTx, BtcTransaction.Type transactionType) throws BitcoinException {
		final int sigVersion = transactionType == BtcTransaction.Type.TRANSACTION_TYPE_LEGACY || transactionType == BtcTransaction.Type.TRANSACTION_TYPE_BITCOIN_CASH ?
				Script.SIGVERSION_BASE : Script.SIGVERSION_WITNESS_V0;
		final BtcTransaction.Input[] signedInputs = new BtcTransaction.Input[unsignedTx.getInputs().length];
		byte hashType = Script.SIGHASH_ALL;
		if (transactionType == BtcTransaction.Type.TRANSACTION_TYPE_BITCOIN_CASH) {
			hashType |= Script.SIGHASH_FORKID;
		}
		byte[][][] witnesses;
		if (sigVersion == Script.SIGVERSION_BASE) {
			witnesses = new byte[0][][];
		} else {
			witnesses = new byte[signedInputs.length][][];
			Arrays.fill(witnesses, new byte[0][]);
		}
		for (int i = 0; i < signedInputs.length; i++) {
			final UnspentOutputInfo outputToSpend = outputsToSpend.get(i);
			final long inputValue = outputToSpend.value;
			final BigInteger privateKey = outputToSpend.keys.privateKey.getPrivateKeyDecoded();
			final byte[] subScript = outputToSpend.scriptPubKey.bytes; //unsignedTx.inputs[i].scriptSig.bytes;

			Script scriptSig;
			if (outputToSpend.scriptPubKey.isPay2PublicKeyHash()) {
				final byte[] signatureAndHashType = getSignatureAndHashType(unsignedTx, i, inputValue, privateKey, subScript, Script.SIGVERSION_BASE, hashType);
				if (outputToSpend.keys.publicKey == null) {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Public key is null");
				}
				scriptSig = new Script(signatureAndHashType, outputToSpend.keys.publicKey);
			} else if (outputToSpend.scriptPubKey.isPubkey()) {
				final byte[] signatureAndHashType = getSignatureAndHashType(unsignedTx, i, inputValue, privateKey, subScript, Script.SIGVERSION_BASE, hashType);
				scriptSig = new Script(Script.convertDataToScript(signatureAndHashType));
			} else if (sigVersion != Script.SIGVERSION_BASE) {
				Script.WitnessProgram wp;
				if (outputToSpend.scriptPubKey.isPayToScriptHash()) {
					if (outputToSpend.keys.publicKey != null && outputToSpend.keys.publicKey.length > 33) {
						throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Writing uncompressed public key into witness");
					}
					wp = new Script.WitnessProgram(0, BtcService.sha256ripemd160(outputToSpend.keys.publicKey));
					scriptSig = new Script(Script.convertDataToScript(wp.getBytes()));
				} else {
					wp = outputToSpend.scriptPubKey.getWitnessProgram();
					scriptSig = new Script(new byte[0]);
				}
				byte[] actualSubScriptForWitness;
				if (wp != null) {
					try {
						final ByteArrayOutputStream os = new ByteArrayOutputStream();
						if (wp.program.length == 20) {
							os.write(Script.OP_DUP);
							os.write(Script.OP_HASH160);
							os.write(Script.convertDataToScript(wp.program));
							os.write(Script.OP_EQUALVERIFY);
							os.write(Script.OP_CHECKSIG);
						} else {
							throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Unsupported scriptPubKey type: " + outputToSpend.scriptPubKey);
						}
						os.close();
						actualSubScriptForWitness = os.toByteArray();
					} catch (final IOException e) {
						throw new RuntimeException(e);
					}
				} else {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Unsupported scriptPubKey type: " + outputToSpend.scriptPubKey);
				}
				final byte[] signatureAndHashType = getSignatureAndHashType(unsignedTx, i, inputValue, privateKey, actualSubScriptForWitness, sigVersion, hashType);
				if (outputToSpend.keys.publicKey == null) {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Writing null public key into witness");
				}
				if (outputToSpend.keys.publicKey.length > 33) {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Writing uncompressed public key into witness");
				}
				witnesses[i] = new byte[][]{signatureAndHashType, outputToSpend.keys.publicKey};
			} else {
				//is it legacy P2SH?
				throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Unsupported scriptPubKey type: " + outputToSpend.scriptPubKey + " for base sig version");
			}
			signedInputs[i] = new BtcTransaction.Input(unsignedTx.getInputs()[i].getOutPoint(), scriptSig, unsignedTx.getInputs()[i].getSequence());
		}
		return new BtcTransaction(1, signedInputs, unsignedTx.getOutputs(), unsignedTx.getLockTime(), witnesses);
	}

	private static byte[] getSignatureAndHashType(BtcTransaction unsignedTx, int i, long inputValue, BigInteger privateKey, byte[] subScript, int sigVersion, byte hashType) {
		final byte[] hash = Script.hashTransaction(i, subScript, unsignedTx, hashType, inputValue, sigVersion);
		final byte[] signature = sign(privateKey, hash);
		final byte[] signatureAndHashType = new byte[signature.length + 1];
		System.arraycopy(signature, 0, signatureAndHashType, 0, signature.length);
		signatureAndHashType[signatureAndHashType.length - 1] = hashType;
		return signatureAndHashType;
	}


	private static FeeChangeAndSelectedOutputs calcFeeChangeAndSelectOutputsToSpend(List<UnspentOutputInfo> unspentOutputs, long amountToSend, float satoshisPerVirtualByte) throws BitcoinException {
		final boolean isPublicKeyCompressed = true;
		long fee = 0;//calculated below
		long change = 0;
		long valueOfUnspentOutputs;
		final ArrayList<UnspentOutputInfo> outputsToSpend = new ArrayList<>();
		if (amountToSend <= 0) {
			//transfer all funds from these addresses to outputAddress
			valueOfUnspentOutputs = 0;
			for (final UnspentOutputInfo outputInfo : unspentOutputs) {
				outputsToSpend.add(outputInfo);
				valueOfUnspentOutputs += outputInfo.value;
			}
			final int txLen = BtcService.getMaximumTxSize(unspentOutputs, 1, isPublicKeyCompressed);
			fee = BtcService.calcMinimumFee(txLen, satoshisPerVirtualByte);
			amountToSend = valueOfUnspentOutputs - fee;
		} else {
			valueOfUnspentOutputs = 0;
			for (final UnspentOutputInfo outputInfo : unspentOutputs) {
				outputsToSpend.add(outputInfo);
				valueOfUnspentOutputs += outputInfo.value;
				long updatedFee = BtcService.calcMinimumFee(150, satoshisPerVirtualByte);
				for (int i = 0; i < 3; i++) {
					fee = updatedFee;
					change = valueOfUnspentOutputs - fee - amountToSend;
					final int txLen = BtcService.getMaximumTxSize(unspentOutputs, change > 0 ? 2 : 1, isPublicKeyCompressed);
					updatedFee = BtcService.calcMinimumFee(txLen, satoshisPerVirtualByte);
					if (updatedFee == fee) {
						break;
					}
				}
				fee = updatedFee;
				if (valueOfUnspentOutputs >= amountToSend + fee) {
					break;
				}
			}

		}
		if (amountToSend > valueOfUnspentOutputs - fee) {
			throw new BitcoinException(BitcoinException.ERR_INSUFFICIENT_FUNDS, "Not enough funds", valueOfUnspentOutputs - fee);
		}
		if (outputsToSpend.isEmpty()) {
			throw new BitcoinException(BitcoinException.ERR_NO_INPUT, "No outputs to spend");
		}
		if (fee > MAX_ALLOWED_FEE) {
			throw new BitcoinException(BitcoinException.ERR_FEE_IS_TOO_BIG, "Fee is too big", fee);
		}
		if (fee < 0) {
			throw new BitcoinException(BitcoinException.ERR_FEE_IS_LESS_THEN_ZERO, "Incorrect fee", fee);
		}
		if (change < 0) {
			throw new BitcoinException(BitcoinException.ERR_CHANGE_IS_LESS_THEN_ZERO,
					"Incorrect change: " + BtcService.formatValue(change), change);
		}
		if (amountToSend < 0) {
			throw new BitcoinException(BitcoinException.ERR_AMOUNT_TO_SEND_IS_LESS_THEN_ZERO,
					"Fees are higher than amount to send: " + BtcService.formatValue(fee), amountToSend);
		}
		return new FeeChangeAndSelectedOutputs(fee, change, amountToSend, outputsToSpend);

	}

	public static byte[] generatePublicKey(BigInteger privateKey, boolean compressed) {
		synchronized (EC_PARAMS) {
			final ECPoint uncompressed = EC_PARAMS.getG().multiply(privateKey);
			return uncompressed.getEncoded(compressed);
		}
	}

	public static byte[] doubleSha256(byte[] bytes) {
		try {
			final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			return sha256.digest(sha256.digest(bytes));
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] sha256(byte[] bytes) {
		try {
			final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			return sha256.digest(bytes);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static String formatValue(long value) throws NumberFormatException {
		if (value < 0) {
			throw new NumberFormatException("Negative value " + value);
		}
		final StringBuilder sb = new StringBuilder(Long.toString(value));
		while (sb.length() <= 8) {
			sb.insert(0, '0');
		}
		sb.insert(sb.length() - 8, '.');
		while (sb.length() > 1 && (sb.charAt(sb.length() - 1) == '0' || sb.charAt(sb.length() - 1) == '.')) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	public static String bip38GetIntermediateCode(String password) throws InterruptedException {
		try {
			final byte[] ownerSalt = new byte[8];
			SECURE_RANDOM.nextBytes(ownerSalt);
			final byte[] passFactor = SCrypt.generate(password.getBytes("UTF-8"), ownerSalt, 16384, 8, 8, 32);
			final ECPoint uncompressed = EC_PARAMS.getG().multiply(new BigInteger(1, passFactor));
			final byte[] passPoint = uncompressed.getEncoded(true);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(fromValidHex("2CE9B3E1FF39E253"));
			baos.write(ownerSalt);
			baos.write(passPoint);
			baos.write(doubleSha256(baos.toByteArray()), 0, 4);
			return Base58.encode(baos.toByteArray());
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static KeyPair bip38GenerateKeyPair(String intermediateCode) throws InterruptedException, BitcoinException {
		final byte[] intermediateBytes = Base58.decode(intermediateCode);
		if (intermediateBytes == null || !verifyDoubleSha256Checksum(intermediateBytes) || intermediateBytes.length != 53) {
			throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Bad intermediate code");
		}
		final byte[] magic = fromValidHex("2CE9B3E1FF39E2");
		for (int i = 0; i < magic.length; i++) {
			if (magic[i] != intermediateBytes[i]) {
				throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE, "It isn't an intermediate code");
			}
		}
		try {
			final byte[] ownerEntropy = new byte[8];
			System.arraycopy(intermediateBytes, 8, ownerEntropy, 0, 8);
			final byte[] passPoint = new byte[33];
			System.arraycopy(intermediateBytes, 16, passPoint, 0, 33);
			final byte flag = (byte) 0x20; //compressed public key
			final byte[] seedB = new byte[24];
			SECURE_RANDOM.nextBytes(seedB);
			final byte[] factorB = doubleSha256(seedB);
			final BigInteger factorBInteger = new BigInteger(1, factorB);
			final ECPoint uncompressedPublicKeyPoint = EC_PARAMS.getCurve().decodePoint(passPoint).multiply(factorBInteger);
			final byte[] publicKey = uncompressedPublicKeyPoint.getEncoded(true);
			final String address = Address.publicKeyToAddress(publicKey);
			final byte[] addressHashAndOwnerSalt = new byte[12];

			final byte[] addressHash = new byte[4];
			System.arraycopy(doubleSha256(address.getBytes("UTF-8")), 0, addressHash, 0, 4);
			System.arraycopy(addressHash, 0, addressHashAndOwnerSalt, 0, 4);
			System.arraycopy(ownerEntropy, 0, addressHashAndOwnerSalt, 4, 8);
			final byte[] derived = SCrypt.generate(passPoint, addressHashAndOwnerSalt, 1024, 1, 1, 64);
			final byte[] key = new byte[32];
			System.arraycopy(derived, 32, key, 0, 32);
			for (int i = 0; i < 16; i++) {
				seedB[i] ^= derived[i];
			}
			final AESEngine cipher = new AESEngine();
			cipher.init(true, new KeyParameter(key));
			final byte[] encryptedHalf1 = new byte[16];
			final byte[] encryptedHalf2 = new byte[16];
			cipher.processBlock(seedB, 0, encryptedHalf1, 0);
			final byte[] secondBlock = new byte[16];
			System.arraycopy(encryptedHalf1, 8, secondBlock, 0, 8);
			System.arraycopy(seedB, 16, secondBlock, 8, 8);
			for (int i = 0; i < 16; i++) {
				secondBlock[i] ^= derived[i + 16];
			}
			cipher.processBlock(secondBlock, 0, encryptedHalf2, 0);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(0x01);
			baos.write(0x43);
			baos.write(flag);
			baos.write(addressHashAndOwnerSalt);
			baos.write(encryptedHalf1, 0, 8);
			baos.write(encryptedHalf2);
			baos.write(doubleSha256(baos.toByteArray()), 0, 4);
			final String encryptedPrivateKey = Base58.encode(baos.toByteArray());

			final byte[] pointB = generatePublicKey(factorBInteger, true);
			final byte pointBPrefix = (byte) (pointB[0] ^ (derived[63] & 0x01));
			final byte[] encryptedPointB = new byte[33];
			encryptedPointB[0] = pointBPrefix;
			for (int i = 0; i < 32; i++) {
				pointB[i + 1] ^= derived[i];
			}
			cipher.processBlock(pointB, 1, encryptedPointB, 1);
			cipher.processBlock(pointB, 17, encryptedPointB, 17);
			baos.reset();
			baos.write(0x64);
			baos.write(0x3B);
			baos.write(0xF6);
			baos.write(0xA8);
			baos.write(0x9A);
			baos.write(flag);
			baos.write(addressHashAndOwnerSalt);
			baos.write(encryptedPointB);
			baos.write(doubleSha256(baos.toByteArray()), 0, 4);
			final String confirmationCode = Base58.encode(baos.toByteArray());

			final Bip38PrivateKeyInfo privateKeyInfo = new Bip38PrivateKeyInfo(encryptedPrivateKey, confirmationCode, true);
			return new KeyPair(address, publicKey, privateKeyInfo);

		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static String bip38DecryptConfirmation(String confirmationCode, String password) throws BitcoinException, InterruptedException {
		final byte[] confirmationBytes = Base58.decode(confirmationCode);
		if (!verifyDoubleSha256Checksum(confirmationBytes) || confirmationBytes.length != 55) {
			throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Bad confirmation code");
		}
		final byte[] magic = fromValidHex("643BF6A89A");
		for (int i = 0; i < magic.length; i++) {
			if (magic[i] != confirmationBytes[i]) {
				throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE, "It isn't a confirmation code");
			}
		}
		try {
			final byte flag = confirmationBytes[5];
			final boolean compressed = (flag & 0x20) == 0x20;
			final boolean lotSequencePresent = (flag & 0x04) == 0x04;
			final byte[] addressHash = new byte[4];
			System.arraycopy(confirmationBytes, 6, addressHash, 0, 4);
			final byte[] ownerEntropy = new byte[8];
			System.arraycopy(confirmationBytes, 10, ownerEntropy, 0, 8);
			final byte[] salt = new byte[lotSequencePresent ? 4 : 8];
			System.arraycopy(ownerEntropy, 0, salt, 0, salt.length);
			final byte[] encryptedPointB = new byte[33];
			System.arraycopy(confirmationBytes, 18, encryptedPointB, 0, 33);
			final byte[] passFactor = SCrypt.generate(password.getBytes("UTF-8"), salt, 16384, 8, 8, 32);
			final ECPoint uncompressed = EC_PARAMS.getG().multiply(new BigInteger(1, passFactor));
			final byte[] passPoint = uncompressed.getEncoded(true);

			final byte[] addressHashAndOwnerSalt = new byte[12];
			System.arraycopy(addressHash, 0, addressHashAndOwnerSalt, 0, 4);
			System.arraycopy(ownerEntropy, 0, addressHashAndOwnerSalt, 4, 8);
			final byte[] derived = SCrypt.generate(passPoint, addressHashAndOwnerSalt, 1024, 1, 1, 64);
			final byte[] key = new byte[32];
			System.arraycopy(derived, 32, key, 0, 32);
			final AESEngine cipher = new AESEngine();
			cipher.init(false, new KeyParameter(key));

			final byte[] pointB = new byte[33];
			pointB[0] = (byte) (encryptedPointB[0] ^ (derived[63] & 0x01));
			cipher.processBlock(encryptedPointB, 1, pointB, 1);
			cipher.processBlock(encryptedPointB, 17, pointB, 17);

			for (int i = 0; i < 32; i++) {
				pointB[i + 1] ^= derived[i];
			}
			ECPoint uncompressedPublicKey;
			try {
				uncompressedPublicKey = EC_PARAMS.getCurve().decodePoint(pointB).multiply(new BigInteger(1, passFactor));
			} catch (final RuntimeException e) {
				//point b doesn't belong the curve - bad password
				return null;
			}
			final String address = Address.publicKeyToAddress(uncompressedPublicKey.getEncoded(compressed));
			final byte[] decodedAddressHash = doubleSha256(address.getBytes("UTF-8"));
			for (int i = 0; i < 4; i++) {
				if (addressHash[i] != decodedAddressHash[i]) {
					return null;
				}
			}
			return address;
		} catch (final UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String bip38Encrypt(KeyPair keyPair, String password) throws InterruptedException {
		try {
			final byte[] addressHash = new byte[4];
			if (keyPair.address == null || StringUtils.isEmpty(keyPair.address.getAddressString())) {
				throw new RuntimeException("Unknown address");
			}
			System.arraycopy(doubleSha256(keyPair.address.toString().getBytes("UTF-8")), 0, addressHash, 0, 4);
			final byte[] passwordDerived = SCrypt.generate(password.getBytes("UTF-8"), addressHash, 16384, 8, 8, 64);
			final byte[] xor = new byte[32];
			System.arraycopy(passwordDerived, 0, xor, 0, 32);
			final byte[] key = new byte[32];
			System.arraycopy(passwordDerived, 32, key, 0, 32);
			final byte[] privateKeyBytes = getPrivateKeyBytes(keyPair.privateKey.getPrivateKeyDecoded());
			for (int i = 0; i < 32; i++) {
				xor[i] ^= privateKeyBytes[i];
			}
			final AESEngine cipher = new AESEngine();
			cipher.init(true, new KeyParameter(key));
			final byte[] encryptedHalf1 = new byte[16];
			final byte[] encryptedHalf2 = new byte[16];
			cipher.processBlock(xor, 0, encryptedHalf1, 0);
			cipher.processBlock(xor, 16, encryptedHalf2, 0);
			final byte[] result = new byte[43];
			result[0] = 1;
			result[1] = 0x42;
			result[2] = (byte) (keyPair.privateKey.isPublicKeyCompressed() ? 0xe0 : 0xc0);
			System.arraycopy(addressHash, 0, result, 3, 4);
			System.arraycopy(encryptedHalf1, 0, result, 7, 16);
			System.arraycopy(encryptedHalf2, 0, result, 23, 16);
			final MessageDigest digestSha = MessageDigest.getInstance("SHA-256");
			digestSha.update(result, 0, result.length - 4);
			System.arraycopy(digestSha.digest(digestSha.digest()), 0, result, 39, 4);
			return Base58.encode(result);
		} catch (final UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] getPrivateKeyBytes(BigInteger privateKey) {
		final byte[] privateKeyPlainNumber = privateKey.toByteArray();
		final int plainNumbersOffs = privateKeyPlainNumber[0] == 0 ? 1 : 0;
		final byte[] privateKeyBytes = new byte[32];
		System.arraycopy(privateKeyPlainNumber, plainNumbersOffs, privateKeyBytes, privateKeyBytes.length - (privateKeyPlainNumber.length - plainNumbersOffs), privateKeyPlainNumber.length - plainNumbersOffs);
		return privateKeyBytes;
	}

	public static KeyPair bip38Decrypt(String encryptedPrivateKey, String password, Address.Type publicKeyRepresentation) throws InterruptedException, BitcoinException {
		final byte[] encryptedPrivateKeyBytes = Base58.decode(encryptedPrivateKey);
		if (encryptedPrivateKeyBytes != null && encryptedPrivateKey.startsWith("6P") && verifyDoubleSha256Checksum(encryptedPrivateKeyBytes) && encryptedPrivateKeyBytes[0] == 1) {
			try {
				final byte[] addressHash = new byte[4];
				System.arraycopy(encryptedPrivateKeyBytes, 3, addressHash, 0, 4);
				final boolean compressed = (encryptedPrivateKeyBytes[2] & 0x20) == 0x20;
				final AESEngine cipher = new AESEngine();
				if (encryptedPrivateKeyBytes[1] == 0x42) {
					final byte[] encryptedSecret = new byte[32];
					System.arraycopy(encryptedPrivateKeyBytes, 7, encryptedSecret, 0, 32);
					final byte[] passwordDerived = SCrypt.generate(password.getBytes("UTF-8"), addressHash, 16384, 8, 8, 64);
					final byte[] key = new byte[32];

					System.arraycopy(passwordDerived, 32, key, 0, 32);
					cipher.init(false, new KeyParameter(key));
					final byte[] secret = new byte[32];
					cipher.processBlock(encryptedSecret, 0, secret, 0);
					cipher.processBlock(encryptedSecret, 16, secret, 16);
					for (int i = 0; i < 32; i++) {
						secret[i] ^= passwordDerived[i];
					}
					final PrivateKeyInfo privateKeyInfo = new Bip38PrivateKeyInfo(encryptedPrivateKey, new BigInteger(1, secret), password, compressed);
					final KeyPair keyPair = new KeyPair(privateKeyInfo, publicKeyRepresentation);
					final byte[] addressHashCalculated = new byte[4];
					if (keyPair.address == null) {
						throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE, "No SegWit address");
					}
					System.arraycopy(doubleSha256(keyPair.address.getAddressString().getBytes("UTF-8")), 0, addressHashCalculated, 0, 4);
					if (!org.bouncycastle.util.Arrays.areEqual(addressHashCalculated, addressHash)) {
						throw new BitcoinException(BitcoinException.ERR_INCORRECT_PASSWORD, "Bad password");
					}
					return keyPair;
				} else if (encryptedPrivateKeyBytes[1] == 0x43) {
					final byte[] ownerSalt = new byte[8];
					System.arraycopy(encryptedPrivateKeyBytes, 7, ownerSalt, 0, 8);
					final byte[] passFactor = SCrypt.generate(password.getBytes("UTF-8"), ownerSalt, 16384, 8, 8, 32);
					final ECPoint uncompressed = EC_PARAMS.getG().multiply(new BigInteger(1, passFactor));
					final byte[] passPoint = uncompressed.getEncoded(true);
					final byte[] addressHashAndOwnerSalt = new byte[12];
					System.arraycopy(encryptedPrivateKeyBytes, 3, addressHashAndOwnerSalt, 0, 12);
					final byte[] derived = SCrypt.generate(passPoint, addressHashAndOwnerSalt, 1024, 1, 1, 64);
					final byte[] key = new byte[32];
					System.arraycopy(derived, 32, key, 0, 32);
					cipher.init(false, new KeyParameter(key));
					final byte[] decryptedHalf2 = new byte[16];
					cipher.processBlock(encryptedPrivateKeyBytes, 23, decryptedHalf2, 0);
					for (int i = 0; i < 16; i++) {
						decryptedHalf2[i] ^= derived[i + 16];
					}
					final byte[] encryptedHalf1 = new byte[16];
					System.arraycopy(encryptedPrivateKeyBytes, 15, encryptedHalf1, 0, 8);
					System.arraycopy(decryptedHalf2, 0, encryptedHalf1, 8, 8);
					final byte[] decryptedHalf1 = new byte[16];
					cipher.processBlock(encryptedHalf1, 0, decryptedHalf1, 0);
					for (int i = 0; i < 16; i++) {
						decryptedHalf1[i] ^= derived[i];
					}
					final byte[] seedB = new byte[24];
					System.arraycopy(decryptedHalf1, 0, seedB, 0, 16);
					System.arraycopy(decryptedHalf2, 8, seedB, 16, 8);
					final byte[] factorB = doubleSha256(seedB);
					final BigInteger privateKey = new BigInteger(1, passFactor).multiply(new BigInteger(1, factorB)).remainder(EC_PARAMS.getN());
					final PrivateKeyInfo privateKeyInfo = new Bip38PrivateKeyInfo(encryptedPrivateKey, privateKey, password, compressed);

					final KeyPair keyPair = new KeyPair(privateKeyInfo, publicKeyRepresentation);
					if (keyPair.address == null) {
						throw new RuntimeException("Unknown address");
					}
					final byte[] resultedAddressHash = doubleSha256(keyPair.address.getAddressString().getBytes("UTF-8"));
					for (int i = 0; i < 4; i++) {
						if (addressHashAndOwnerSalt[i] != resultedAddressHash[i]) {
							throw new BitcoinException(BitcoinException.ERR_INCORRECT_PASSWORD, "Bad password");
						}
					}
					return keyPair;
				} else {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Bad encrypted private key");
				}
			} catch (final UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		} else {
			throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE, "It is not an encrypted private key");
		}
	}

}