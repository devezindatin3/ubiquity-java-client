package com.gitlab.blockdaemon.ubiquity.tx.eth;

import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.utils.Numeric;

import com.gitlab.blockdaemon.ubiquity.model.SignedTx;


public class EthTransactionBuilder {
	private RawTransaction rawTransaction;
	private int chainId = 1;
	private Credentials credentials;  

	public EthTransactionBuilder rawTransaction(RawTransaction rawTransaction) {
		this.rawTransaction = rawTransaction;
		return this;
	}

	public EthTransactionBuilder chainId(int chainId) {
		this.chainId = chainId;
		return this;
	}

	public EthTransactionBuilder credentials(Credentials credentials) {
		this.credentials = credentials;
		return this;
	}

	public SignedTx build() {
		byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, chainId, credentials);
		String hexValue = Numeric.toHexString(signedMessage).replaceFirst("^0x", "");
		return new SignedTx().tx(hexValue);
	}
}
