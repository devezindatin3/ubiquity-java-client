package com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions;

public final class NotImplementedException extends RuntimeException {

	private static final long serialVersionUID = 3093291793260228637L;

	public NotImplementedException(String message) {
	        super(message);
	}
}
