package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.util.ArrayList;
import java.util.List;

import com.gitlab.blockdaemon.ubiquity.model.SignedTx;
import com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions.BitcoinException;

public class BtcTransactionBuilder {

	private List<UnspentOutputInfo> unspentOutputs;
	private String outputAddress;
	private String changeAddress;
	private long amountToSend = -1;
	private float satoshisPerVirtualByte;
	private BtcTransaction.Type type = BtcTransaction.Type.TRANSACTION_TYPE_LEGACY;

	public BtcTransactionBuilder fee(String satoshisPerVirtualByte) {
		this.satoshisPerVirtualByte = Float.parseFloat(satoshisPerVirtualByte);
		return this;
	}

	public BtcTransactionBuilder unspentOutputs(List<UnspentOutputInfo> unspentOutputs) {
		this.unspentOutputs = unspentOutputs;
		return this;
	}

	public BtcTransactionBuilder addUnspentOutput(UnspentOutputInfo unspentOutput) {
		if(this.unspentOutputs == null) {
			this.unspentOutputs = new ArrayList<>();
		}
		this.unspentOutputs.add(unspentOutput);
		return this;
	}


	public BtcTransactionBuilder changeAddress(String changeAddress) {
		this.changeAddress = changeAddress;
		return this;
	}

	public BtcTransactionBuilder outputAddress(String outputAddress) {
		this.outputAddress = outputAddress;
		return this;
	}

	public BtcTransactionBuilder satoshisPerVirtualByte(String satoshisPerVirtualByte) {
		this.satoshisPerVirtualByte = Float.parseFloat(satoshisPerVirtualByte);
		return this;
	}

	public BtcTransactionBuilder transactionType(BtcTransaction.Type type) {
		this.type = type;
		return this;
	}

	public BtcTransactionBuilder amountToSend(long amountToSend) {
		this.amountToSend = amountToSend;
		return this;
	}

	public SignedTx build() throws BitcoinException {
		final BtcTransaction tx = BtcService.createTransaction(this.unspentOutputs, this.outputAddress, this.changeAddress, this.amountToSend, this.satoshisPerVirtualByte, this.type);
		return new SignedTx().tx(tx.toHexEncodedString());
	}
}
