/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 2.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

package com.gitlab.blockdaemon.ubiquity.wrappers;

import com.gitlab.blockdaemon.ubiquity.api.BlocksApi;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiResponse;
import com.gitlab.blockdaemon.ubiquity.model.Block;
import com.gitlab.blockdaemon.ubiquity.model.BlockIdentifier;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;

public class BlocksApiWrapper {
	private BlocksApi api;

	public BlocksApiWrapper(BlocksApi api) {
		this.api = api;
	}

	public BlocksApi raw() {
		return api;
	}

	/**
	 * Block By Number/Hash Get a block and all its transactions by the block number
	 * or hash
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @param key      Block number or block hash/ID or Special identifier
	 *                 (required)
	 * @return Block {@link Block}
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 * 
	 */
	public Block getBlock(Platform platform, Network network, String key) throws ApiException {
		ApiResponse<Block> localVarResp = this.api.getBlockWithHttpInfo(platform.getPlatform(), network.getNetwork(),
				key);
		return localVarResp.getData();
	}

	/**
	 * Block By Number Get a block and all its transactions by the block number or
	 * hash
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @param key      Block number (required)
	 * @return Block {@link Block}
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 * 
	 */
	public Block getBlock(Platform platform, Network network, Integer height) throws ApiException {
		ApiResponse<Block> localVarResp = this.api.getBlockWithHttpInfo(platform.getPlatform(), network.getNetwork(),
				height.toString());
		return localVarResp.getData();
	}

	/**
	 * Block Identifier By Number/Hash Get minimal block identifier by block number
	 * or hash
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @param key      Block number or block hash/ID or Special identifier
	 *                 (required)
	 * @return BlockIdentifier {@link BlockIdentifier}
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 */
	public BlockIdentifier getBlockIdentifier(Platform platform, Network network, String key) throws ApiException {
		ApiResponse<BlockIdentifier> localVarResp = this.api.getBlockIdentifierWithHttpInfo(platform.getPlatform(),
				network.getNetwork(), key);
		return localVarResp.getData();
	}

	/**
	 * Block Identifier By Number Get minimal block identifier by block number
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @param key      Block number (required)
	 * @return BlockIdentifier {@link BlockIdentifier}
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 */
	public BlockIdentifier getBlockIdentifier(Platform platform, Network network, Integer height) throws ApiException {
		ApiResponse<BlockIdentifier> localVarResp = this.api.getBlockIdentifierWithHttpInfo(platform.getPlatform(),
				network.getNetwork(), height.toString());
		return localVarResp.getData();
	}
}
