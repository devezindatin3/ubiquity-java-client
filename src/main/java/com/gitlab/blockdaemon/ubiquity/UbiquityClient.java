package com.gitlab.blockdaemon.ubiquity;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;
import com.gitlab.blockdaemon.ubiquity.api.BlocksApi;
import com.gitlab.blockdaemon.ubiquity.api.PlatformsApi;
import com.gitlab.blockdaemon.ubiquity.api.SyncApi;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.wrappers.AccountsApiWrapper;
import com.gitlab.blockdaemon.ubiquity.wrappers.BlocksApiWrapper;
import com.gitlab.blockdaemon.ubiquity.wrappers.PlatformsApiWrapper;
import com.gitlab.blockdaemon.ubiquity.wrappers.SyncApiWrapper;
import com.gitlab.blockdaemon.ubiquity.wrappers.TransactionsApiWrapper;
import com.gitlab.blockdaemon.ubiquity.ws.Notification;
import com.gitlab.blockdaemon.ubiquity.ws.NotificationDeserializer;
import com.gitlab.blockdaemon.ubiquity.ws.WebsocketClient;

public class UbiquityClient {

	private final ApiClient apiClient;
	private final BlocksApiWrapper block;
	private final PlatformsApiWrapper platform;
	private final AccountsApiWrapper accounts;
	private final TransactionsApiWrapper transactions;
	private final SyncApiWrapper sync;

	public UbiquityClient(final ApiClient apiClient) {
		this.block = new BlocksApiWrapper(new BlocksApi(apiClient));
		this.platform = new PlatformsApiWrapper(new PlatformsApi(apiClient));
		this.accounts = new AccountsApiWrapper(new AccountsApi(apiClient));
		this.transactions = new TransactionsApiWrapper(new TransactionsApi(apiClient));
		this.sync = new SyncApiWrapper(new SyncApi(apiClient));
		this.apiClient = apiClient;

		final SimpleModule module =	new SimpleModule("PolymorphicNotificationDeserializerModule", new Version(1, 0, 0, "", "",""));
		module.addDeserializer(Notification.class,  new NotificationDeserializer());
		apiClient.getJSON().getMapper().registerModule(module);
	}

	public BlocksApiWrapper block() {
		return this.block;
	}

	public PlatformsApiWrapper platform() {
		return this.platform;
	}

	public AccountsApiWrapper accounts() {
		return this.accounts;
	}

	public TransactionsApiWrapper transactions() {
		return this.transactions;
	}

	public SyncApiWrapper sync() {
		return this.sync;
	}

	public WebsocketClient ws(Platform bitcoin, Network mainNet) {
		return new WebsocketClient(this.apiClient, bitcoin, mainNet);
	}
}
