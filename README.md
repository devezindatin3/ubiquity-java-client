# Ubiquity-Java-Client

An client to the ubiquity service of blockdaemon.com
 

#Development
This project uses https://projectlombok.org/setup/gradle and is designed for JDK 8+

# Usage

## Basic

```java
 UbiquityClient client = new UbiquityClientBuilder().authBearerToken("token").build();
 
 Block block = client.block().getBlock(Platforms.BITCOIN, Networks.MAIN_NET, "current");
```

An API URL may also be specified if you have a personal ubiquity endpoint 

```java
 UbiquityClient client = new UbiquityClientBuilder().api("url").authBearerToken("token").build();
 
 Block block = client.block().getBlock(Platforms.BITCOIN, Networks.MAIN_NET, "current");
```


## Paginated API's

Certain resources contain more data than can practically returned in a single request. In these resources the data is split across multiple responses where each response returns a subset of the items requested and a continuation token. Requests for the first page of data should not contain a continuation token. To get the next batch of items the continuation token should be passed with the subsequent request. If no continuation token is returned all of the available data has been returned.

Initial request to paged API's should not include a continuation. If no limit is supplied the default of 25 will be applied.

```java
TxPage txPage1 = client.transactions().getTxs(Platform.ETHEREUM, Network.MAIN_NET, Order.DESC, "", null, null);
```

To continue through the pages of transactions the continuation from the previous page must be supplied to the next request

```java
String continuationFromPreviousPage = txPage1.getContinuation();
TxPage txPage2 = client.transactions().getTxs(Platform.ETHEREUM, Network.MAIN_NET, Order.DESC, continuationFromPreviousPage);
```

## Custom networks and platforms
New protocols or networks may not be present always have enums explicitly defined within in the client 
However these protocols and networks can be accessed via the name method.

```java
Block block = client.block().getBlock(Platform.name("algorand"), Network.name("mainnet"), 685700);
```
 

Further examples of are provided in the [example project](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/UbiquityClientExample.java)

# Sending transactions
Ubiquity can send signed transactions which can be created by the client for both Bitcoin and Ethereum. For Bitcoin the signed transaction can be created using the functionality within the client.

```java
	UbiquityClient client = new UbiquityClientBuilder().authBearerToken("UBIQUITY_AUTH_TOKEN").build();

	String privateKey = "cTLFPEWG1CppcM7Q8DmrPNcr4TpqPxXccVotCauQV2qNLNmPqvc2";
	String hashOfPrevTransaction = "d0921672675bd1b086ed959f8e6a81d0acacfc5c9853e2c34536a844f9bb66df";
	String changeAddress = "msb9xrxbDwnoofQ1VdinHRRg1zoPt42JHc";
	String outputAddress = "msEZfkxjgn423BmPc72d6i1fbeit2sCRLD";

	PrivateKeyInfo privateKeyInfo = BtcUtils.decodePrivateKey(privateKey);
	KeyPair keyPair = new KeyPair(privateKeyInfo, Address.Type.PUBLIC_KEY_TO_ADDRESS_LEGACY);

	// Specify the unspent output of the transactions as well as the value to use as
	// input the the current transaction
	UnspentOutputInfo unspentOutput = new UnspentOutputInfo()
			.keys(keyPair)
			.txHash(hashOfPrevTransaction)
			.value(1462631l)
			.outputIndex(0)
			.build();

	// An estimate for the fee can be retrieved from the Ubiquity API
	String suggestedFee = client.transactions().estimateFee(Platform.BITCOIN, Network.TEST_NET);

	// Create a signed transaction
	SignedTx signedTx = new BtcTransactionBuilder()
			.fee(suggestedFee)
			.amountToSend(1000000l)
			.changeAddress(changeAddress)
			.outputAddress(outputAddress)
			.addUnspentOutput(unspentOutput)
			.build();

	// Send the signed transaction to the ubiquity API and receive the id of the
	// transaction
	TxReceipt receipt = client.transactions().txSend(Platform.BITCOIN, Network.TEST_NET, signedTx);
```


For Ethereum signed transactions are created using the web3j library

```java
	UbiquityClient client = new UbiquityClientBuilder().authBearerToken("UBIQUITY_AUTH_TOKEN").build();

	String fromPrivateKey = "91d75b8d4411a4cce7be104f3b4c149e583f1c2a6fdea78ad3498597ae997ed3";
	Credentials credentials = Credentials.create(fromPrivateKey);

	String toPublicKey = "0x32Ceb3C8409742A446c012Ed94DFa5672c28692B";
	BigInteger value = Convert.toWei("1.0", Convert.Unit.ETHER).toBigInteger();
	BigInteger nonce = BigInteger.valueOf(4);

	// An estimate for the fee can be retrieved from the Ubiquity API
	String suggestedFee = client.transactions().estimateFee(Platform.ETHEREUM, Network.name("ropsten"), 10);

	BigInteger gasprice = new BigInteger(suggestedFee);
	BigInteger gaslimit = BigInteger.valueOf(21000);

	// To prevent replay attacks the chain id should also be included
	// https://chainid.network/
	int chainId = 3;

	// Signed transactions can be created using the web3j library
	RawTransaction rawTransaction = RawTransaction.createEtherTransaction(nonce, gasprice, gaslimit, toPublicKey, value);

	// A signed ubiquity tx can the be created through the EthTransactionBuilder
	// class
	SignedTx tx = new EthTransactionBuilder()
			.chainId(chainId)
			.credentials(credentials)
			.rawTransaction(rawTransaction)
			.build();

	// The signed transaction can then be sent using the ubiquity API
	TxReceipt receipt = client.transactions().txSend(Platform.ETHEREUM, Network.name("ropsten"), tx);
	System.out.println(receipt.getId());
```
Further examples of are provided in the example project [Bitcoin](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/BtcTransactionExample.java)
[Ethereum](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/EthTransactionExample.java)

# Websockets
Create a new websocket client using try with resources so the connection will be gracefully closed after use.
The client implements autoclosable so that it may be used with try with resources  closing the socket gracefully may take a few seconds

There are currently three channels Tx, Block and BlockIdentifiers. The focus of the notifications may additionally be refined using the details section
For example, the following code restricts the Tx channel notifications to only those about address "0x123".
 
```java
	private static BiConsumer<WebsocketClient, TxNotification> txConsumer = new BiConsumer<>() {
		@Override
		public void accept(WebsocketClient t, TxNotification u) {
			System.out.println(u);
		}
	};

	try (WebsocketClient wsClient = client.ws(Platform.ETHEREUM, Network.MAIN_NET);) {
		final Subscription<TxNotification> subscription = Subscription.tx(singletonMap("addresses", singletonList("0x123")), txConsumer);
		wsClient.subscribe(subscription);
	}	
```

If the connection to the server is lost the connection will be retried every 10 seconds by default this can be configured on the client using the setReconnectDelay method.

When the websocket is closed gracefully already-enqueued messages will continue to be transmitted however new messaged will be refused.


Further examples of are provided in the [example project](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/WebsocketExample.java)
 

# Dependency

Add a repositories section to your build.gradle file:
	
```
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/26939001/packages/maven"
        name "ubiquity-client-gitLab"
    }
}
```
	
```
dependencies {
		implementation group: "com.gitlab.blockdaemon", name: "ubiquity-client", version: "0.0.2"
		implementation "org.web3j:core:4.8.4"
}
```
	
	
Further explanations of these instructions are given [here](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html) 

 
