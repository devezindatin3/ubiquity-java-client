# AccountsApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBalancesByAddress**](AccountsApi.md#getBalancesByAddress) | **GET** /v2/{platform}/{network}/account/{address} | Balances Of Address
[**getBalancesByAddresses**](AccountsApi.md#getBalancesByAddresses) | **POST** /v2/{platform}/{network}/accounts | Balances Of Addresses
[**getListOfBalancesByAddress**](AccountsApi.md#getListOfBalancesByAddress) | **GET** /v1/{platform}/{network}/account/{address} | Balances Of Address
[**getListOfBalancesByAddresses**](AccountsApi.md#getListOfBalancesByAddresses) | **POST** /v1/{platform}/{network}/accounts | Balances Of Addresses
[**getReportByAddress**](AccountsApi.md#getReportByAddress) | **GET** /v1/{platform}/{network}/account/{address}/report | A financial report for an address between a time period. Default timescale is within the last 30 days
[**getTxsByAddress**](AccountsApi.md#getTxsByAddress) | **GET** /v1/{platform}/{network}/account/{address}/txs | Transactions Of Address
[**v2GetReportByAddress**](AccountsApi.md#v2GetReportByAddress) | **GET** /v2/{platform}/{network}/account/{address}/report | A financial report for an address between a time period. Default timescale is within the last 30 days
[**v2GetTxsByAddress**](AccountsApi.md#v2GetTxsByAddress) | **GET** /v2/{platform}/{network}/account/{address}/txs | Transactions Of Address



## getBalancesByAddress

> Map&lt;String, Object&gt; getBalancesByAddress(platform, network, address, assets)

Balances Of Address

Returns the account balances for all supported currencies.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        String assets = "ethereum/native/eth"; // String | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect.
        try {
            Map<String, Object> result = apiInstance.getBalancesByAddress(platform, network, address, assets);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getBalancesByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **assets** | **String**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | [optional]

### Return type

**Map&lt;String, Object&gt;**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Balances |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getBalancesByAddresses

> Map&lt;String, Map&lt;String, Object&gt;&gt; getBalancesByAddresses(platform, network, accountsObj, assets)

Balances Of Addresses

Returns the balances of accounts for all supported currencies.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        AccountsObj accountsObj = new AccountsObj(); // AccountsObj | 
        String assets = "ethereum/native/eth"; // String | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect.
        try {
            Map<String, Map<String, Object>> result = apiInstance.getBalancesByAddresses(platform, network, accountsObj, assets);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getBalancesByAddresses");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **accountsObj** | [**AccountsObj**](AccountsObj.md)|  |
 **assets** | **String**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | [optional]

### Return type

[**Map&lt;String, Map&lt;String, Object&gt;&gt;**](Map.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Balances |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getListOfBalancesByAddress

> List&lt;BalanceV1&gt; getListOfBalancesByAddress(platform, network, address)

Balances Of Address

Returns the account balances for all supported currencies.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        try {
            List<BalanceV1> result = apiInstance.getListOfBalancesByAddress(platform, network, address);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getListOfBalancesByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |

### Return type

[**List&lt;BalanceV1&gt;**](BalanceV1.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Balances |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getListOfBalancesByAddresses

> Map&lt;String, List&lt;BalanceV1&gt;&gt; getListOfBalancesByAddresses(platform, network, accountsObj)

Balances Of Addresses

Returns the balances of accounts for all supported currencies.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        AccountsObj accountsObj = new AccountsObj(); // AccountsObj | 
        try {
            Map<String, List<BalanceV1>> result = apiInstance.getListOfBalancesByAddresses(platform, network, accountsObj);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getListOfBalancesByAddresses");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **accountsObj** | [**AccountsObj**](AccountsObj.md)|  |

### Return type

[**Map&lt;String, List&lt;BalanceV1&gt;&gt;**](List.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Balances |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getReportByAddress

> Report getReportByAddress(platform, network, address, from, to, limit, continuation)

A financial report for an address between a time period. Default timescale is within the last 30 days

Returns account activity


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        Integer from = 961846434; // Integer | Unix Timestamp from where to start
        Integer to = 1119612834; // Integer | Unix Timestamp from where to end
        Integer limit = 1000; // Integer | Max number of items to return in a response. Defaults to 50k and is capped at 100k. 
        String continuation = "xyz"; // String | Continuation token from earlier response
        try {
            Report result = apiInstance.getReportByAddress(platform, network, address, from, to, limit, continuation);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getReportByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **from** | **Integer**| Unix Timestamp from where to start | [optional]
 **to** | **Integer**| Unix Timestamp from where to end | [optional]
 **limit** | **Integer**| Max number of items to return in a response. Defaults to 50k and is capped at 100k.  | [optional]
 **continuation** | **String**| Continuation token from earlier response | [optional]

### Return type

[**Report**](Report.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Account Activity |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |
| **413** | Too Many Transactions |  -  |


## getTxsByAddress

> TxPageV1 getTxsByAddress(platform, network, address, order, continuation, limit, assets)

Transactions Of Address

Gets transactions that an address was involved with, from newest to oldest.
This call uses pagination.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        String order = "order_example"; // String | Pagination order
        String continuation = "8185.123"; // String | Continuation token from earlier response
        Integer limit = 25; // Integer | Max number of items to return in a response. Defaults to 25 and is capped at 100. 
        String assets = "ethereum/native/eth"; // String | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect.
        try {
            TxPageV1 result = apiInstance.getTxsByAddress(platform, network, address, order, continuation, limit, assets);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getTxsByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **order** | **String**| Pagination order | [optional] [enum: desc, asc]
 **continuation** | **String**| Continuation token from earlier response | [optional]
 **limit** | **Integer**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]
 **assets** | **String**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | [optional]

### Return type

[**TxPageV1**](TxPageV1.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transactions |  -  |
| **400** | Invalid address |  -  |
| **401** | Invalid or expired token |  -  |
| **403** | Invalid continuation |  -  |
| **429** | Rate limit exceeded |  -  |


## v2GetReportByAddress

> Report v2GetReportByAddress(platform, network, address, from, to, limit, continuation)

A financial report for an address between a time period. Default timescale is within the last 30 days

Returns account activity


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        Integer from = 961846434; // Integer | Unix Timestamp from where to start
        Integer to = 1119612834; // Integer | Unix Timestamp from where to end
        Integer limit = 1000; // Integer | Max number of items to return in a response. Defaults to 50k and is capped at 100k. 
        String continuation = "xyz"; // String | Continuation token from earlier response
        try {
            Report result = apiInstance.v2GetReportByAddress(platform, network, address, from, to, limit, continuation);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#v2GetReportByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **from** | **Integer**| Unix Timestamp from where to start | [optional]
 **to** | **Integer**| Unix Timestamp from where to end | [optional]
 **limit** | **Integer**| Max number of items to return in a response. Defaults to 50k and is capped at 100k.  | [optional]
 **continuation** | **String**| Continuation token from earlier response | [optional]

### Return type

[**Report**](Report.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Account Activity |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |
| **413** | Too Many Transactions |  -  |


## v2GetTxsByAddress

> TxPage v2GetTxsByAddress(platform, network, address, order, continuation, limit, assets)

Transactions Of Address

Gets transactions that an address was involved with, from newest to oldest.
This call uses pagination.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        String order = "order_example"; // String | Pagination order
        String continuation = "8185.123"; // String | Continuation token from earlier response
        Integer limit = 25; // Integer | Max number of items to return in a response. Defaults to 25 and is capped at 100. 
        String assets = "ethereum/native/eth"; // String | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect.
        try {
            TxPage result = apiInstance.v2GetTxsByAddress(platform, network, address, order, continuation, limit, assets);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#v2GetTxsByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **order** | **String**| Pagination order | [optional] [enum: desc, asc]
 **continuation** | **String**| Continuation token from earlier response | [optional]
 **limit** | **Integer**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]
 **assets** | **String**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | [optional]

### Return type

[**TxPage**](TxPage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transactions |  -  |
| **400** | Invalid address |  -  |
| **401** | Invalid or expired token |  -  |
| **403** | Invalid continuation |  -  |
| **429** | Rate limit exceeded |  -  |

