# SyncApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**currentBlockID**](SyncApi.md#currentBlockID) | **GET** /v2/{platform}/{network}/sync/block_id | Get current block ID
[**currentBlockNumber**](SyncApi.md#currentBlockNumber) | **GET** /v2/{platform}/{network}/sync/block_number | Get current block number



## currentBlockID

> String currentBlockID(platform, network)

Get current block ID

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.SyncApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        SyncApi apiInstance = new SyncApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        try {
            String result = apiInstance.currentBlockID(platform, network);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling SyncApi#currentBlockID");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |

### Return type

**String**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Current block ID |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## currentBlockNumber

> Long currentBlockNumber(platform, network)

Get current block number

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.SyncApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        SyncApi apiInstance = new SyncApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        try {
            Long result = apiInstance.currentBlockNumber(platform, network);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling SyncApi#currentBlockNumber");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |

### Return type

**Long**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Current block number |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |

