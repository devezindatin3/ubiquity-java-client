

# Utxo

An unspent transaction output

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetPath** | **String** | Asset path of transferred currency |  [optional]
**address** | **String** |  |  [optional]
**value** | **String** | Integer string in smallest unit (Satoshis) |  [optional]



