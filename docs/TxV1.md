

# TxV1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique transaction identifier |  [optional]
**date** | **Long** | Unix timestamp |  [optional]
**blockId** | **String** | ID of block if mined, otherwise omitted. |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | Result status of the transaction. |  [optional]
**nonce** | **Integer** |  |  [optional]
**numEvents** | **Integer** |  |  [optional]
**meta** | **Object** |  |  [optional]
**events** | [**List&lt;Event&gt;**](Event.md) |  |  [optional]



## Enum: StatusEnum

Name | Value
---- | -----
COMPLETED | &quot;completed&quot;
FAILED | &quot;failed&quot;



