

# Coin


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | **String** | Platform handle (platform var in path) |  [optional]
**slip44** | **Integer** | SatoshiLabs 0044, registered coin types: https://github.com/satoshilabs/slips/blob/master/slip-0044.md |  [optional]
**symbol** | **String** | Symbol of native currency |  [optional]
**name** | **String** | Name of platform |  [optional]
**blockTime** | **Integer** | Average time between blocks (milliseconds) |  [optional]
**sampleAddress** | **String** | Random address seen on chain (optional) |  [optional]



