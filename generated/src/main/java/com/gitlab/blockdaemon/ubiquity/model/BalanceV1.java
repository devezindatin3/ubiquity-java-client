/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Protocols #### Mainnet The following protocols are currently supported: * bitcoin * ethereum * polkadot * xrp * algorand * stellar * dogecoin * oasis * near * terra * litecoin * bitcoincash  #### Testnet * bitcoin/testnet * ethereum/ropsten * dogecoin/testnet * litecoin/testnet * bitcoincash/testnet  #### Native Ubiquity provides native access to all Blockchain nodes it supports. To access native functionality, use the protocol without the v2 prefix * bitcoin/(mainnet | testnet) - [RPC Documentation](https://developer.bitcoin.org/reference/rpc/) * ethereum/(mainnet | ropsten) - [RPC Documentation](https://ethereum.org/en/developers/docs/apis/json-rpc/) * polkadot/mainnet - [Sidecar API Documentation](https://paritytech.github.io/substrate-api-sidecar/dist/) * polkadot/mainnet/http-rpc - [Polkadot RPC Documentation](https://polkadot.js.org/docs/substrate/rpc/) * algorand/mainnet - [Algod API Documentation](https://developer.algorand.org/docs/reference/rest-apis/algod/v1/) * stellar/mainnet - [Stellar Horizon API Documentation](https://developers.stellar.org/api) * dogecoin/(mainnet | testnet) - [Dogecoin API Documentaion](https://developer.bitcoin.org/reference/rpc/) * oasis/mainnet - [Oasis Rosetta Gateway Documentation](https://www.rosetta-api.org/docs/api_identifiers.html#network-identifier) * near/mainnet - [NEAR RPC Documentation](https://docs.near.org/docs/api/rpc) * terra/mainnet - [Terra RPC Documentation](https://docs.terra.money/docs/develop/how-to/endpoints.html) * litecoin/mainnet - [Litecoin RPC Documentation](https://litecoin.info/index.php/Litecoin_API) * bitcoincash/mainnet - [Bitcoin Cash RPC Documentation](https://docs.bitcoincashnode.org/doc/json-rpc/)  A full URL example: https://ubiquity.api.blockdaemon.com/bitcoin/mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 2.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.gitlab.blockdaemon.ubiquity.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import com.gitlab.blockdaemon.ubiquity.model.Currency;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.gitlab.blockdaemon.ubiquity.invoker.JSON;


/**
 * Currency balances with asset paths as keys
 */
@ApiModel(description = "Currency balances with asset paths as keys")
@JsonPropertyOrder({
  BalanceV1.JSON_PROPERTY_CURRENCY,
  BalanceV1.JSON_PROPERTY_CONFIRMED_BALANCE,
  BalanceV1.JSON_PROPERTY_PENDING_BALANCE,
  BalanceV1.JSON_PROPERTY_CONFIRMED_NONCE,
  BalanceV1.JSON_PROPERTY_CONFIRMED_BLOCK
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class BalanceV1 {
  public static final String JSON_PROPERTY_CURRENCY = "currency";
  private Currency currency;

  public static final String JSON_PROPERTY_CONFIRMED_BALANCE = "confirmed_balance";
  private String confirmedBalance;

  public static final String JSON_PROPERTY_PENDING_BALANCE = "pending_balance";
  private String pendingBalance;

  public static final String JSON_PROPERTY_CONFIRMED_NONCE = "confirmed_nonce";
  private Integer confirmedNonce;

  public static final String JSON_PROPERTY_CONFIRMED_BLOCK = "confirmed_block";
  private Integer confirmedBlock;


  public BalanceV1 currency(Currency currency) {
    this.currency = currency;
    return this;
  }

   /**
   * Get currency
   * @return currency
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_CURRENCY)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Currency getCurrency() {
    return currency;
  }


  @JsonProperty(JSON_PROPERTY_CURRENCY)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setCurrency(Currency currency) {
    this.currency = currency;
  }


  public BalanceV1 confirmedBalance(String confirmedBalance) {
    this.confirmedBalance = confirmedBalance;
    return this;
  }

   /**
   * Get confirmedBalance
   * @return confirmedBalance
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_CONFIRMED_BALANCE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public String getConfirmedBalance() {
    return confirmedBalance;
  }


  @JsonProperty(JSON_PROPERTY_CONFIRMED_BALANCE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setConfirmedBalance(String confirmedBalance) {
    this.confirmedBalance = confirmedBalance;
  }


  public BalanceV1 pendingBalance(String pendingBalance) {
    this.pendingBalance = pendingBalance;
    return this;
  }

   /**
   * Get pendingBalance
   * @return pendingBalance
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_PENDING_BALANCE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public String getPendingBalance() {
    return pendingBalance;
  }


  @JsonProperty(JSON_PROPERTY_PENDING_BALANCE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setPendingBalance(String pendingBalance) {
    this.pendingBalance = pendingBalance;
  }


  public BalanceV1 confirmedNonce(Integer confirmedNonce) {
    this.confirmedNonce = confirmedNonce;
    return this;
  }

   /**
   * Get confirmedNonce
   * @return confirmedNonce
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_CONFIRMED_NONCE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Integer getConfirmedNonce() {
    return confirmedNonce;
  }


  @JsonProperty(JSON_PROPERTY_CONFIRMED_NONCE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setConfirmedNonce(Integer confirmedNonce) {
    this.confirmedNonce = confirmedNonce;
  }


  public BalanceV1 confirmedBlock(Integer confirmedBlock) {
    this.confirmedBlock = confirmedBlock;
    return this;
  }

   /**
   * Get confirmedBlock
   * @return confirmedBlock
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_CONFIRMED_BLOCK)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Integer getConfirmedBlock() {
    return confirmedBlock;
  }


  @JsonProperty(JSON_PROPERTY_CONFIRMED_BLOCK)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setConfirmedBlock(Integer confirmedBlock) {
    this.confirmedBlock = confirmedBlock;
  }


  /**
   * Return true if this balance-v1 object is equal to o.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BalanceV1 balanceV1 = (BalanceV1) o;
    return Objects.equals(this.currency, balanceV1.currency) &&
        Objects.equals(this.confirmedBalance, balanceV1.confirmedBalance) &&
        Objects.equals(this.pendingBalance, balanceV1.pendingBalance) &&
        Objects.equals(this.confirmedNonce, balanceV1.confirmedNonce) &&
        Objects.equals(this.confirmedBlock, balanceV1.confirmedBlock);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currency, confirmedBalance, pendingBalance, confirmedNonce, confirmedBlock);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BalanceV1 {\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    confirmedBalance: ").append(toIndentedString(confirmedBalance)).append("\n");
    sb.append("    pendingBalance: ").append(toIndentedString(pendingBalance)).append("\n");
    sb.append("    confirmedNonce: ").append(toIndentedString(confirmedNonce)).append("\n");
    sb.append("    confirmedBlock: ").append(toIndentedString(confirmedBlock)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

