/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Protocols #### Mainnet The following protocols are currently supported: * bitcoin * ethereum * polkadot * xrp * algorand * stellar * dogecoin * oasis * near * terra * litecoin * bitcoincash  #### Testnet * bitcoin/testnet * ethereum/ropsten * dogecoin/testnet * litecoin/testnet * bitcoincash/testnet  #### Native Ubiquity provides native access to all Blockchain nodes it supports. To access native functionality, use the protocol without the v2 prefix * bitcoin/(mainnet | testnet) - [RPC Documentation](https://developer.bitcoin.org/reference/rpc/) * ethereum/(mainnet | ropsten) - [RPC Documentation](https://ethereum.org/en/developers/docs/apis/json-rpc/) * polkadot/mainnet - [Sidecar API Documentation](https://paritytech.github.io/substrate-api-sidecar/dist/) * polkadot/mainnet/http-rpc - [Polkadot RPC Documentation](https://polkadot.js.org/docs/substrate/rpc/) * algorand/mainnet - [Algod API Documentation](https://developer.algorand.org/docs/reference/rest-apis/algod/v1/) * stellar/mainnet - [Stellar Horizon API Documentation](https://developers.stellar.org/api) * dogecoin/(mainnet | testnet) - [Dogecoin API Documentaion](https://developer.bitcoin.org/reference/rpc/) * oasis/mainnet - [Oasis Rosetta Gateway Documentation](https://www.rosetta-api.org/docs/api_identifiers.html#network-identifier) * near/mainnet - [NEAR RPC Documentation](https://docs.near.org/docs/api/rpc) * terra/mainnet - [Terra RPC Documentation](https://docs.terra.money/docs/develop/how-to/endpoints.html) * litecoin/mainnet - [Litecoin RPC Documentation](https://litecoin.info/index.php/Litecoin_API) * bitcoincash/mainnet - [Bitcoin Cash RPC Documentation](https://docs.bitcoincashnode.org/doc/json-rpc/)  A full URL example: https://ubiquity.api.blockdaemon.com/bitcoin/mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 2.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.gitlab.blockdaemon.ubiquity.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import com.gitlab.blockdaemon.ubiquity.model.MultiTransfer;
import com.gitlab.blockdaemon.ubiquity.model.MultiTransferOperation;
import com.gitlab.blockdaemon.ubiquity.model.TransferOperation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.gitlab.blockdaemon.ubiquity.invoker.JSON;

import com.fasterxml.jackson.core.type.TypeReference;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.gitlab.blockdaemon.ubiquity.invoker.JSON;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
@JsonDeserialize(using = Operation.OperationDeserializer.class)
@JsonSerialize(using = Operation.OperationSerializer.class)
public class Operation extends AbstractOpenApiSchema {
    private static final Logger log = Logger.getLogger(Operation.class.getName());

    public static class OperationSerializer extends StdSerializer<Operation> {
        public OperationSerializer(Class<Operation> t) {
            super(t);
        }

        public OperationSerializer() {
            this(null);
        }

        @Override
        public void serialize(Operation value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
            jgen.writeObject(value.getActualInstance());
        }
    }

    public static class OperationDeserializer extends StdDeserializer<Operation> {
        public OperationDeserializer() {
            this(Operation.class);
        }

        public OperationDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public Operation deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            JsonNode tree = jp.readValueAsTree();
            Object deserialized = null;
            Operation newOperation = new Operation();
            Map<String,Object> result2 = tree.traverse(jp.getCodec()).readValueAs(new TypeReference<Map<String, Object>>() {});
            String discriminatorValue = (String)result2.get("type");
            switch (discriminatorValue) {
                case "multi_transfer":
                    deserialized = tree.traverse(jp.getCodec()).readValueAs(MultiTransferOperation.class);
                    newOperation.setActualInstance(deserialized);
                    return newOperation;
                case "multi_transfer_operation":
                    deserialized = tree.traverse(jp.getCodec()).readValueAs(MultiTransferOperation.class);
                    newOperation.setActualInstance(deserialized);
                    return newOperation;
                case "transfer":
                    deserialized = tree.traverse(jp.getCodec()).readValueAs(TransferOperation.class);
                    newOperation.setActualInstance(deserialized);
                    return newOperation;
                case "transfer_operation":
                    deserialized = tree.traverse(jp.getCodec()).readValueAs(TransferOperation.class);
                    newOperation.setActualInstance(deserialized);
                    return newOperation;
                default:
                    log.log(Level.WARNING, String.format("Failed to lookup discriminator value `%s` for Operation. Possible values: multi_transfer multi_transfer_operation transfer transfer_operation", discriminatorValue));
            }

            boolean typeCoercion = ctxt.isEnabled(MapperFeature.ALLOW_COERCION_OF_SCALARS);
            int match = 0;
            JsonToken token = tree.traverse(jp.getCodec()).nextToken();
            // deserialize MultiTransferOperation
            try {
                boolean attemptParsing = true;
                // ensure that we respect type coercion as set on the client ObjectMapper
                if (MultiTransferOperation.class.equals(Integer.class) || MultiTransferOperation.class.equals(Long.class) || MultiTransferOperation.class.equals(Float.class) || MultiTransferOperation.class.equals(Double.class) || MultiTransferOperation.class.equals(Boolean.class) || MultiTransferOperation.class.equals(String.class)) {
                    attemptParsing = typeCoercion;
                    if (!attemptParsing) {
                        attemptParsing |= ((MultiTransferOperation.class.equals(Integer.class) || MultiTransferOperation.class.equals(Long.class)) && token == JsonToken.VALUE_NUMBER_INT);
                        attemptParsing |= ((MultiTransferOperation.class.equals(Float.class) || MultiTransferOperation.class.equals(Double.class)) && token == JsonToken.VALUE_NUMBER_FLOAT);
                        attemptParsing |= (MultiTransferOperation.class.equals(Boolean.class) && (token == JsonToken.VALUE_FALSE || token == JsonToken.VALUE_TRUE));
                        attemptParsing |= (MultiTransferOperation.class.equals(String.class) && token == JsonToken.VALUE_STRING);
                    }
                }
                if (attemptParsing) {
                    deserialized = tree.traverse(jp.getCodec()).readValueAs(MultiTransferOperation.class);
                    // TODO: there is no validation against JSON schema constraints
                    // (min, max, enum, pattern...), this does not perform a strict JSON
                    // validation, which means the 'match' count may be higher than it should be.
                    match++;
                    log.log(Level.FINER, "Input data matches schema 'MultiTransferOperation'");
                }
            } catch (Exception e) {
                // deserialization failed, continue
                log.log(Level.FINER, "Input data does not match schema 'MultiTransferOperation'", e);
            }

            // deserialize TransferOperation
            try {
                boolean attemptParsing = true;
                // ensure that we respect type coercion as set on the client ObjectMapper
                if (TransferOperation.class.equals(Integer.class) || TransferOperation.class.equals(Long.class) || TransferOperation.class.equals(Float.class) || TransferOperation.class.equals(Double.class) || TransferOperation.class.equals(Boolean.class) || TransferOperation.class.equals(String.class)) {
                    attemptParsing = typeCoercion;
                    if (!attemptParsing) {
                        attemptParsing |= ((TransferOperation.class.equals(Integer.class) || TransferOperation.class.equals(Long.class)) && token == JsonToken.VALUE_NUMBER_INT);
                        attemptParsing |= ((TransferOperation.class.equals(Float.class) || TransferOperation.class.equals(Double.class)) && token == JsonToken.VALUE_NUMBER_FLOAT);
                        attemptParsing |= (TransferOperation.class.equals(Boolean.class) && (token == JsonToken.VALUE_FALSE || token == JsonToken.VALUE_TRUE));
                        attemptParsing |= (TransferOperation.class.equals(String.class) && token == JsonToken.VALUE_STRING);
                    }
                }
                if (attemptParsing) {
                    deserialized = tree.traverse(jp.getCodec()).readValueAs(TransferOperation.class);
                    // TODO: there is no validation against JSON schema constraints
                    // (min, max, enum, pattern...), this does not perform a strict JSON
                    // validation, which means the 'match' count may be higher than it should be.
                    match++;
                    log.log(Level.FINER, "Input data matches schema 'TransferOperation'");
                }
            } catch (Exception e) {
                // deserialization failed, continue
                log.log(Level.FINER, "Input data does not match schema 'TransferOperation'", e);
            }

            if (match == 1) {
                Operation ret = new Operation();
                ret.setActualInstance(deserialized);
                return ret;
            }
            throw new IOException(String.format("Failed deserialization for Operation: %d classes match result, expected 1", match));
        }

        /**
         * Handle deserialization of the 'null' value.
         */
        @Override
        public Operation getNullValue(DeserializationContext ctxt) throws JsonMappingException {
            throw new JsonMappingException(ctxt.getParser(), "Operation cannot be null");
        }
    }

    // store a list of schema names defined in oneOf
    public static final Map<String, GenericType> schemas = new HashMap<String, GenericType>();

    public Operation() {
        super("oneOf", Boolean.FALSE);
    }

    public Operation(MultiTransferOperation o) {
        super("oneOf", Boolean.FALSE);
        setActualInstance(o);
    }

    public Operation(TransferOperation o) {
        super("oneOf", Boolean.FALSE);
        setActualInstance(o);
    }

    static {
        schemas.put("MultiTransferOperation", new GenericType<MultiTransferOperation>() {
        });
        schemas.put("TransferOperation", new GenericType<TransferOperation>() {
        });
        JSON.registerDescendants(Operation.class, Collections.unmodifiableMap(schemas));
        // Initialize and register the discriminator mappings.
        Map<String, Class<?>> mappings = new HashMap<String, Class<?>>();
        mappings.put("multi_transfer", MultiTransferOperation.class);
        mappings.put("multi_transfer_operation", MultiTransferOperation.class);
        mappings.put("transfer", TransferOperation.class);
        mappings.put("transfer_operation", TransferOperation.class);
        mappings.put("operation", Operation.class);
        JSON.registerDiscriminator(Operation.class, "type", mappings);
    }

    @Override
    public Map<String, GenericType> getSchemas() {
        return Operation.schemas;
    }

    /**
     * Set the instance that matches the oneOf child schema, check
     * the instance parameter is valid against the oneOf child schemas:
     * MultiTransferOperation, TransferOperation
     *
     * It could be an instance of the 'oneOf' schemas.
     * The oneOf child schemas may themselves be a composed schema (allOf, anyOf, oneOf).
     */
    @Override
    public void setActualInstance(Object instance) {
        if (JSON.isInstanceOf(MultiTransferOperation.class, instance, new HashSet<Class<?>>())) {
            super.setActualInstance(instance);
            return;
        }

        if (JSON.isInstanceOf(TransferOperation.class, instance, new HashSet<Class<?>>())) {
            super.setActualInstance(instance);
            return;
        }

        throw new RuntimeException("Invalid instance type. Must be MultiTransferOperation, TransferOperation");
    }

    /**
     * Get the actual instance, which can be the following:
     * MultiTransferOperation, TransferOperation
     *
     * @return The actual instance (MultiTransferOperation, TransferOperation)
     */
    @Override
    public Object getActualInstance() {
        return super.getActualInstance();
    }

    /**
     * Get the actual instance of `MultiTransferOperation`. If the actual instanct is not `MultiTransferOperation`,
     * the ClassCastException will be thrown.
     *
     * @return The actual instance of `MultiTransferOperation`
     * @throws ClassCastException if the instance is not `MultiTransferOperation`
     */
    public MultiTransferOperation getMultiTransferOperation() throws ClassCastException {
        return (MultiTransferOperation)super.getActualInstance();
    }

    /**
     * Get the actual instance of `TransferOperation`. If the actual instanct is not `TransferOperation`,
     * the ClassCastException will be thrown.
     *
     * @return The actual instance of `TransferOperation`
     * @throws ClassCastException if the instance is not `TransferOperation`
     */
    public TransferOperation getTransferOperation() throws ClassCastException {
        return (TransferOperation)super.getActualInstance();
    }

}

